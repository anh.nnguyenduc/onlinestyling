﻿namespace Project_Prn211_4.Models
{
    public class RegisterViewModel
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string RealName { get; set; }
    }
}
