﻿using System;
using System.Collections.Generic;

namespace Project_Prn211_4.Models
{
    public partial class Address
    {
        public int AddressId { get; set; }
        public int UserId { get; set; }
        public string AddressLine1 { get; set; } = null!;
        public string? AddressLine2 { get; set; }
        public string City { get; set; } = null!;
        public string State { get; set; } = null!;
        public string Country { get; set; } = null!;

        public virtual User User { get; set; } = null!;
    }
}
