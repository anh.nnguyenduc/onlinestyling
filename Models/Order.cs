﻿using System;
using System.Collections.Generic;

namespace Project_Prn211_4.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int OrderId { get; set; }
        public int UserId { get; set; }
        public DateTime OrderDate { get; set; }
        public string? Address { get; set; }
        public string? PaymentMethod { get; set; }
        public decimal? TotalAmount { get; set; }

        public virtual User User { get; set; } = null!;
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
