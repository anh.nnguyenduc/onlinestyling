﻿using Project_Prn211_4.Models;

namespace Project_Prn211_4.ViewModels
{
	public class OrderViewModel
	{
		public Order Order { get; set; }
		public List<OrderDetail> OrderDetails { get; set; }
	}
}