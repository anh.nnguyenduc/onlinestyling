﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Project_Prn211_4.Models;
using Project_Prn211_4.ViewModels;
using System.Transactions;

namespace Project_Prn211_4.Controllers
{
	public class HomeController : Controller
	{
		public IActionResult Index(int? categoryId)
		{
			Project_PRN211_4Context context = new Project_PRN211_4Context();
			IQueryable<Product> products = context.Products.Include(p => p.Category);

			if (categoryId.HasValue)
			{
				products = products.Where(p => p.CategoryId == categoryId.Value);
			}
			else
			{
				ViewBag.StockMessage = "Out of stock";
			}
			var filteredProducts = products.ToList();

			var categories = context.Categories.ToList();

			ViewBag.SelectedCategoryId = categoryId;
			ViewBag.Categories = categories;

			return View(filteredProducts);
		}
		public IActionResult IndexAdmin(int? categoryId)
		{
			Project_PRN211_4Context context = new Project_PRN211_4Context();
			IQueryable<Product> products = context.Products.Include(p => p.Category);

			if (categoryId.HasValue)
			{
				products = products.Where(p => p.CategoryId == categoryId.Value);
			}
			else
			{
				ViewBag.StockMessage = "Out of stock";
			}
			var filteredProducts = products.ToList();

			var categories = context.Categories.ToList();
			ViewBag.Categories = categories;

			ViewBag.SelectedCategoryId = categoryId;

			return View(filteredProducts);
		}
		public IActionResult Login()
		{
			return View();
		}
		[HttpPost]
		public IActionResult Login(User u)
		{
			string user = u.Username;
			string password = u.Password;
			Project_PRN211_4Context context = new Project_PRN211_4Context();
			var userDb = context.Users.FirstOrDefault(x => x.Username == user && x.Password == password);
			if (userDb != null && userDb.Role != 1)
			{
				HttpContext.Session.SetString("u", userDb.UserId.ToString());
				return RedirectToAction("Index", "Home");

			}
			else if (userDb != null && userDb.Role == 1)
			{
				HttpContext.Session.SetString("u", userDb.UserId.ToString());
				return RedirectToAction("IndexAdmin", "Home");
			}
			ModelState.AddModelError(string.Empty, "Credential is invalid!!");
			return View(u);
		}
		public IActionResult Logout()
		{
			HttpContext.Session.Remove("u");
			return RedirectToAction("Login", "Home");
		}
		public IActionResult Register()
		{
			return View();
		}
		[HttpPost]
		public IActionResult Register(RegisterViewModel model)
		{
			Project_PRN211_4Context context = new Project_PRN211_4Context();
			if (ModelState.IsValid)
			{
				var existingUser = context.Users.FirstOrDefault(u => u.Username == model.Username);
				if (existingUser != null)
				{
					ViewBag.ErrorMessage = "Username is already existed!!";
					return View(model);
				}

				var newUser = new User
				{
					Username = model.Username,
					Email = model.Email,
					Password = model.Password,
					RealName = model.RealName,
				};

				context.Users.Add(newUser);
				context.SaveChanges();

				return RedirectToAction("Login");
			}

			return View(new User());
		}


		public IActionResult Cart(string message)
		{
			if (message != null)
			{
				ViewBag.QuantityMessage = message;
			}
			int userId = int.Parse(HttpContext.Session.GetString("u"));
			using (var context = new Project_PRN211_4Context())
			{
				var cartItems = context.Carts
				.Where(u => u.UserId == userId)
					.Include(c => c.Product)
					.ToList();
				ViewData["userId"] = userId;
				return View(cartItems);
			}
		}
		[HttpPost]
		public IActionResult AddToCart(int productId)
		{
			using (var context = new Project_PRN211_4Context())
			{
				int userId = int.Parse(HttpContext.Session.GetString("u"));
				var user = context.Users.FirstOrDefault(u => u.UserId == userId);
				if (user != null)
				{
					var product = context.Products.FirstOrDefault(p => p.ProductId == productId);
					if (product != null && product.Quantity > 0)
					{
						var cartItem = context.Carts.FirstOrDefault(c => c.UserId == userId && c.ProductId == productId);
						if (cartItem != null)
						{
							cartItem.Quantity += 1;
						}
						else
						{
							var newCartItem = new Cart
							{
								UserId = userId,
								ProductId = productId,
								Quantity = 1,

							};
							context.Carts.Add(newCartItem);
						}
						context.SaveChanges();
					}
				}
			}
			return RedirectToAction("Cart");
		}

		public IActionResult Profile(string message)
		{
			if (message != null)
			{
				ViewBag.msg = message;
			}
			var userId = int.Parse(HttpContext.Session.GetString("u"));
			ViewData["UserId"] = userId;
			using (var context = new Project_PRN211_4Context())
			{
				var user = context.Users.FirstOrDefault(u => u.UserId == userId);
				if (user != null)
				{
					return View(user);
				}
			}
			return View();
		}

		public IActionResult UpdateProfile()
		{
			var userId = int.Parse(HttpContext.Session.GetString("u"));
			using (var context = new Project_PRN211_4Context())
			{
				var user = context.Users.FirstOrDefault(u => u.UserId == userId);
				return View(user);
			}
		}

		[HttpPost]
		public IActionResult UpdateProfile(IFormCollection collection)
		{
			var userId = int.Parse(HttpContext.Session.GetString("u"));
			using (var context = new Project_PRN211_4Context())
			{
				var user = context.Users.FirstOrDefault(u => u.UserId == userId);
				if (user != null && collection["oldPass"] == user.Password)
				{
					if(collection["newPass"] == collection["RenewPass"])
					{
						user.Password = collection["newPass"];
						context.SaveChanges();
					}
					else
					{
						string error1 = "New password and re-enter do not match. Enter again!!!";
						ViewBag.error1 = error1;
						return View(user);
					}
					
				}
				else
				{
					string error2 = "Wrong password. Enter again!!!";
					ViewBag.error2 = error2;
					return View(user);
				}
				string msg = "Update success";
				ViewBag.MessageSuccess = msg;
				return View(user);
			}
		}
		public IActionResult UpdateProfile2()
		{
			var userId = int.Parse(HttpContext.Session.GetString("u"));
			using (var context = new Project_PRN211_4Context())
			{
				var user = context.Users.FirstOrDefault(u => u.UserId == userId);
				return View(user);
			}
		}
		[HttpPost]
		public IActionResult UpdateProfile2(IFormCollection collection)
		{
			var userId = int.Parse(HttpContext.Session.GetString("u"));
			using (var context = new Project_PRN211_4Context())
			{
				var user = context.Users.FirstOrDefault(u => u.UserId == userId);
				if (user != null)
				{
					user.Email = collection["Email"];
					user.RealName = collection["RealName"];
					context.SaveChanges();
				}
				string msg = "Update success";
				ViewBag.MessageSuccess = msg;
				return View(user);
			}
		}
		public IActionResult Delete(string id)
		{
			using (var context = new Project_PRN211_4Context())
			{
				int userId = int.Parse(HttpContext.Session.GetString("u"));
				var user = context.Users.FirstOrDefault(u => u.UserId == userId);
				int productId = int.Parse(id);
				if (user != null)
				{
					var cartItem = context.Carts.FirstOrDefault(c => c.UserId == userId && c.ProductId == productId);
					if (cartItem != null)
					{
						cartItem.Quantity -= 1;
						if (cartItem.Quantity <= 0)
						{
							context.Carts.Remove(cartItem);
						}
					}
					else
					{
						ModelState.AddModelError(string.Empty, "Products is invalid");
					}
					context.SaveChanges();
				}
			}
			return RedirectToAction("Cart");
		}

		[HttpPost]
		public IActionResult Order(string shippingAddress, string paymentMethod)
		{
			var userId = int.Parse(HttpContext.Session.GetString("u"));

			using (var context = new Project_PRN211_4Context())
			{
				using (var transaction = context.Database.BeginTransaction())
				{
					try
					{
						var cartItems = context.Carts
							.Where(c => c.UserId == userId)
							.Include(c => c.Product)
							.ToList();
						decimal? total = 0;
						foreach (var cartItem in cartItems)
						{
							if (cartItem.Product.Quantity == 0)
							{
								ModelState.AddModelError(string.Empty, "This product is temporarily out of stock");
								return RedirectToAction("Cart");
							}
							var product = cartItem.Product;
							if (cartItem.Quantity > product.Quantity)
							{
								string msg = "Quantity invalid, please order less";
								return RedirectToAction("Cart", new { message = msg });
							}
							cartItem.Product.Quantity -= cartItem.Quantity;
							total += cartItem.Product.Price * cartItem.Quantity;
						}

						var order = new Order
						{
							UserId = userId,
							Address = shippingAddress,
							PaymentMethod = paymentMethod,
							OrderDate = DateTime.Now,
							TotalAmount = total,
						};
						context.Orders.Add(order);
						context.SaveChanges();

						foreach (var cartItem in cartItems)
						{
							var orderDetail = new OrderDetail
							{
								OrderId = order.OrderId,
								ProductId = cartItem.ProductId,
								Quantity = cartItem.Quantity
							};
							context.OrderDetails.Add(orderDetail);
						}
						context.SaveChanges();

						context.Carts.RemoveRange(cartItems);
						context.SaveChanges();

						// Cập nhật số lượng sản phẩm sau khi đã đặt hàng
						context.SaveChanges();

						transaction.Commit();

						return RedirectToAction("Purchased", new { orderId = order.OrderId });
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						ModelState.AddModelError(string.Empty, "Exception when ordering: " + ex.Message);
					}
				}
			}
			return RedirectToAction("Cart", "Home");
		}




		public IActionResult OrderDetails(string shippingAddress, string paymentMethod)
		{
			var userId = int.Parse(HttpContext.Session.GetString("u"));
			using (var context = new Project_PRN211_4Context())
			{
				var order = new Order
				{
					UserId = userId,
					Address = shippingAddress,
					PaymentMethod = paymentMethod,
					OrderDate = DateTime.Now
				};
				var cartItems = context.Carts
							.Where(c => c.UserId == userId)
							.Include(c => c.Product)
							.ToList();

				var orderViewModel = new OrderViewModel
				{
					Order = order,
					OrderDetails = cartItems.Select(c => new OrderDetail
					{
						ProductId = c.ProductId,
						Quantity = c.Quantity
					}).ToList()
				};

				return View(orderViewModel.OrderDetails);
			}
		}


		public IActionResult Purchased()
		{
			var userId = int.Parse(HttpContext.Session.GetString("u"));
			using (var context = new Project_PRN211_4Context())
			{
				var orders = context.Orders
					.Where(o => o.UserId == userId)
					.Include(o => o.OrderDetails)
					.ThenInclude(od => od.Product)
					.ToList();

				if (orders.Any())
				{
					return View(orders);
				}
				else
				{
					ModelState.AddModelError(string.Empty, "You have not made any purchases yet!!");
					return View(null);
				}
			}
		}

		public IActionResult ProductDetails(int id)
		{
			using (var context = new Project_PRN211_4Context())
			{
				var product = context.Products
					.Include(p => p.Category)
					.FirstOrDefault(p => p.ProductId == id);

				if (product != null)
				{
					return View(product);
				}
			}

			return RedirectToAction("Index", "Home");
		}

		public IActionResult Feedback(int id)
		{
			ViewBag.ProductId = id;
			var userId = int.Parse(HttpContext.Session.GetString("u"));
			ViewBag.UserId = userId;

			using (var context = new Project_PRN211_4Context())
			{
				var review = context.Reviews
					.Include(r => r.Product)
					.Include(r => r.User)
					.Where(r => r.ProductId == id)
					.ToList();

				if (review.Count > 0)
				{
					return View(review);
				}
			}

			return View(new List<Review>());
		}

		public IActionResult CreateFeedback(int productId)
		{
			var model = new Review { ProductId = productId };
			return View("CreateFeedback", model);
		}

		[HttpPost]
		public IActionResult SaveFeedback(Review model)
		{
			var userId = int.Parse(HttpContext.Session.GetString("u"));
			var productId = model.ProductId;
			model.UserId = userId;
			model.ProductId = productId;

			using (var context = new Project_PRN211_4Context())
			{
				context.Reviews.Add(model);
				context.SaveChanges();
			}
			return RedirectToAction("Feedback", new { id = productId });
		}
		public IActionResult UserManage()
		{
			using (var context = new Project_PRN211_4Context())
			{
				var user = context.Users.ToList();
				return View(user);
			}
		}
		public IActionResult DeleteUser(int id)
		{
			using (var context = new Project_PRN211_4Context())
			{
				var user = context.Users.FirstOrDefault(u => u.UserId == id);
				if (user != null)
				{
					// Xóa các bản ghi liên quan trong bảng OrderDetails trước
					var orderDetails = context.OrderDetails
						.Where(od => od.Order.UserId == id)
						.ToList();
					context.OrderDetails.RemoveRange(orderDetails);
					context.SaveChanges();

					// Xóa các bản ghi liên quan trong bảng Orders trước
					var orders = context.Orders.Where(o => o.UserId == id).ToList();
					context.Orders.RemoveRange(orders);
					context.SaveChanges();

					var cartItems = context.Carts
				.Where(c => c.UserId == id)
				.ToList();
					context.Carts.RemoveRange(cartItems);
					context.SaveChanges();

					// Xóa các bản ghi liên quan trong bảng Reviews
					var reviews = context.Reviews
						.Where(r => r.UserId == id)
						.ToList();
					context.Reviews.RemoveRange(reviews);
					context.SaveChanges();

					// Xóa người dùng
					context.Users.Remove(user);
					context.SaveChanges();

					return RedirectToAction("UserManage");
				}
			}
			return RedirectToAction("UserManage");
		}
		public IActionResult ProductManage()
		{
			using (var context = new Project_PRN211_4Context())
			{
				var products = context.Products.ToList();
				return View(products);
			}
		}
		[HttpPost]
		public IActionResult SaveProduct(Product product)
		{
			using (var context = new Project_PRN211_4Context())
			{
				var existingProduct = context.Products.FirstOrDefault(p => p.ProductId == product.ProductId);
				if (existingProduct != null)
				{
					existingProduct.ProductName = product.ProductName;
					existingProduct.Price = product.Price;
					existingProduct.Quantity = product.Quantity;

					context.SaveChanges();
				}
			}

			return RedirectToAction("IndexAdmin");
		}

	}
}
